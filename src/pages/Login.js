
import {useState, useEffect, useContext} from 'react';
//useContext enables us to consume
import {Form,Button} from 'react-bootstrap';
//dala nya yung userProvider si usercontext.provider

import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
//hndi nakcurl kasi  lhat ginagamit
import UserContext from '../UserContext';
//hindi nakacurly brace kasi kabuuan ng UserContecxt ang inextract natin


export default function Login (){

	//Allows us to consume the UserContext object and its properties to use for user validation
	//useContext(Provider);

	const {user, setUser} = useContext(UserContext);
	console.log(user);
	//state hooks to store the values of the input fields
	const [email, setEmail] = useState ('');
	const [password, setPassword] = useState ('');
	//state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	//check if all values from are successfully binded
	console.log(email);
	console.log(password);

	function loginUser(e){
		//prevents page redirection via form submission in any page
		e.preventDefault();

		//syntax:
			//fetch('url',options)

		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers:{
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})		
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			if(typeof data.accessToken !== "undefined"){
				//token is yung key and data.accessToken ang value
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title:"Login Sucessfull!",
					icon:"success",
					text:"Welcome to Booking App of 196!"
				})
			}else{
				Swal.fire({
					title:"Authentication Failed",
					icon: "error",
					text: "Check your credentails!"
				});
			};

		});
		//set the email of the authenticated user in the local storgae
		//Syntax
		//localStorage.setItem('propertyName'/key',value)
		//setItem naglalagay ng laman
		//awww comment out na si email email kasi acccess token lang need
		// localStorage.setItem("email",email);
		//login - clear input fields

		//access user info through local storage to update the user state which will help update the App component and rerender it to avoid rfereshing the page upon user login and logout

		//when state change components are reredndered and the AppNavBar component will be updated based on the credentials -- unlike the previous setItem na magconditional rerednder

		// setUser({
		// 	email: localStorage.getItem('email')
		// 	//gusto natin makuha yng galing sa lcal storage, galing to sa app.js
		// });


		setEmail('');
		setPassword('');

		// alert(`${email} has successfully logged in!`)
	};

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/getUserDetails', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			setUser({
				id:data._id,
				isAdmin:data.isAdmin
			});
		})
	}

	//hook
	useEffect (() => {
		
		if(email !== '' && password !==''){
			setIsActive(true);
		}else{
			setIsActive(false);
		};

	}, [email, password]);

	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>B196 Booking App Login:</h1>
		<Form onSubmit={e=>loginUser(e)}>
			<Form.Group controlId="userloginEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
					/>
					<Form.Text className="text-muted">
						<i>Enter your registered email.</i>
					</Form.Text>
			</Form.Group>

			<Form.Group controlId="userPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here!"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
					/>
					<Form.Text className="text-muted">
						<i>Forgot Password?</i>
					</Form.Text>
			</Form.Group>
			<p>Not yet registered? <Link to="/register">Register Here</Link></p>

		{ isActive ?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submit_Btnlogin">
				Login
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submit_Btnlogin" disabled>
				Login
			</Button>
		}
		</Form>		
		</>
		)
}

/*controlID for input field siya ang nagpoprroduce*/