import React from 'react';


// Creates a Context Object
//context object - data type of an object that can be used to store infromation that can be shared to other components within the app

const UserContext = React.createContext();

//yung ginawa mong object siya yung magiging the one kasi may .Provider na
//Provide component allows other componensts to consume/use the context object and supply the necessary info needed

export const UserProvider = UserContext.Provider;

//para magamit si User Context, export natin
export default UserContext;

