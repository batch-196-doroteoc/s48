import {useState} from 'react';
import {Card,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard ({courseProp}){
	//check to see if data from courseProps was succesfully passed
	// console.log(courseProp);
		//result: php-l (couseData[0])
		//every component receives info in a form of object
	// console.log(typeof props);

	//object destructuring
	const {name, description, price,_id} = courseProp;
	
	//react hooks - useState -> store its state
	//Syntax:
		//const [getter,setter] = useState(initialGetterValue);

	//ACTIVITY with enrolled students///awww commment out
	// const [count, setCount] = useState(10);
	// const [countEnrolled, enrolledCount] = useState(0);

	// function enroll (){
	// 	if(count<1){
	// 		alert(`No more available slots`);
	// 	}else{
	// 	setCount(count - 1);
	// 	enrolledCount(countEnrolled + 1);
	// 	console.log(`Seats Available: ${count}`);
	// 	console.log(`Enrolees: ${countEnrolled}`);
	// 	}
	// };


	return(
				<Card className = "p-3 mb-3">
					<Card.Body>
							<Card.Title className="fw-bold">{name}</Card.Title>
							<Card.Subtitle>Course Description:</Card.Subtitle>
							<Card.Text>
							{description}
							</Card.Text>
							<Card.Subtitle>Course Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
{/*							<Card.Text>Number of Enrolled Students: {countEnrolled}</Card.Text>
							<Card.Text>Slots Available: {count}</Card.Text>
							<Button variant = "primary" onClick={enroll}>Enroll!</Button>*/}
							<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
					</Card.Body>
				</Card>	
		)
};

//onClick serves as an event
//addEventListener('click', e => {
//})